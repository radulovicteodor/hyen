import { Media, News, NewsArticle, Faq } from "./pages";
import { Routes, Route, Navigate } from "react-router-dom";
import Press from "./pages/Press/Press";
import Campaign from "./pages/Campaign/Campaign";

const AppRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<Navigate to="/media" replace={true} />}></Route>
      <Route path="/media" element={<Media />} />
      <Route path="/press" element={<Press />} />
      <Route path="/campaign" element={<Campaign />} />
      <Route path="/news" element={<News />} />
      <Route path="/news/:newsId" element={<NewsArticle />} />
      <Route path="/faq" element={<Faq />} />
    </Routes>
  );
};

export default AppRoutes;
