/* eslint-disable react/prop-types */
import "./Layout.css";

const Layout = (props) => {
  return (
    <div className="main" style={{ marginTop: "64px" }}>
      {props.children}
    </div>
  );
};

export default Layout;
