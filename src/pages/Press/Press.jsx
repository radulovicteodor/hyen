import Content from "./components/Content/Content";
import Hero from "./components/Hero/Hero";
import HeroSections from "./components/Hero/HeroSections/HeroSections";

const Press = () => {
  return (
    <div>
      <Hero />
      <HeroSections />
      <Content />
    </div>
  );
};

export default Press;
