import * as styles from "./Content.module.css";

const Content = () => {
  return (
    <div className={styles.content_container}>
      <div className={styles.content1}>
        <h4>Get early access and sign up to the Alpha</h4>
        <button>sign up for the Alpha</button>
      </div>
      <div className={styles.content2}>
        <p>About</p>
        <p>The game</p>
        <div className={styles.content_card}>
          <span>find out more</span>
        </div>
      </div>
      <div className={styles.content3}>
        <div>
          <p>Join our</p>
          <p>Discord</p>
        </div>
        <div className={styles.content_card}>
          <span>Join the community</span>
        </div>
        <img src="./img/press/discord.svg" />
      </div>
    </div>
  );
};

export default Content;
