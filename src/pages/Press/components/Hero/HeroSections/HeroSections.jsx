import HeroSection from "../HeroSection/HeroSection";
import * as style from "./HeroSections.module.css";

const HeroSections = () => {
  return (
    <div className={style.hero_sections}>
      <div className={style.hero_sections_container}>
        <HeroSection
          title={
            <>
              Download <br />
              Press Pack
            </>
          }
          desc="CA created press pack includes: brand guidelines, logos, screenshots, key art, video links etc.) in a ZIP format"
          btnText="Download (4.2mb .zip)"
          imgSrc="./img/press/Commander_Wright 6.png"
          left="62%"
          bgSrc="./img/press/bg_article2.svg"
        />{" "}
        <HeroSection
          title="PR contact details"
          desc="John Doe
Senior Manager, Communications, Europe
alennuyeux@playhyenas.com"
          btnText="GET IN TOUCH"
          imgSrc="./img/press/Commander_Wright 7.png"
          left="52%"
          bgSrc="./img/press/bg_article.svg"
        />{" "}
        <HeroSection
          title={
            <>
              Content <br />
              Creators Pack
            </>
          }
          desc="CA created press pack includes: brand guidelines, logos, screenshots, key art, video links etc.) in a ZIP format"
          btnText="Download (4.2mb .zip)"
          imgSrc="./img/press/Commander_Wright 8.png"
          left="40%"
          bgSrc="./img/press/bg_article3.svg"
        />
      </div>
    </div>
  );
};

export default HeroSections;
