import * as style from "./HeroSection.module.css";

const HeroSection = ({ title, desc, btnText, imgSrc, left, bgSrc }) => {
  console.log(bgSrc);
  return (
    <div className={style.hero_section_container}>
      <img src={imgSrc} className={style.hero_section_img} style={{ left }} />
      <div
        className={style.hero_section_article}
        style={{
          backgroundImage: `url(${bgSrc})`,
        }}
      >
        <div className={style.hero_section_article_text}>
          <h4>{title}</h4>
          <p>{desc}</p>
        </div>
        <button>{btnText}</button>
      </div>
    </div>
  );
};

export default HeroSection;
