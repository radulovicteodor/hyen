import * as style from "./Faq.module.css";
import FaqHero from "./components/FaqHero/FaqHero";
import Separator from "./components/Separator/Separator";
import MostFaq from "./components/MostFaq/MostFaq";
import FaqAccordion from "./components/FaqAccordion/FaqAccordion";

const Faq = () => {
  return (
    <div className={style.faq}>
      <FaqHero />
      <Separator />
      <MostFaq />
      <FaqAccordion />
    </div>
  );
};

export default Faq;
