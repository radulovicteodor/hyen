import * as style from "./FaqAccordion.module.css";
import Panel from "./components/Panel";

import img1 from "../../../../assets/images/faq/panel1.jpg";
import img2 from "../../../../assets/images/faq/panel2.jpg";
import img3 from "../../../../assets/images/faq/panel3.jpg";

const PanelContentWrapper = ({ children }) => {
  return <div className={style.panel_content}>{children}</div>;
};

const FaqAccordion = () => {
  return (
    <div className={style.faq_accordion}>
      <h4>
        <span>All other </span> questions
      </h4>
      <Panel title="Who can sign up?">
        <PanelContentWrapper>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu
            turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus
            nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum
            tellus elit sed risus. Maecenas eget condimentum velit, sit amet
            feugiat lectus. Class aptent taciti sociosqu ad litora torquent per
            conubia nostra, per inceptos himenaeos. Praesent auctor purus luctus
            enim egestas, ac scelerisque ante pulvinar. Donec ut rhoncus ex.
            Suspendisse ac rhoncus nisl, eu tempor urna. Curabitur vel bibendum
            lorem. Morbi convallis convallis diam sit amet lacinia. Aliquam in
            elementum tellus. Nam pulvinar blandit velit, id condimentum diam
            faucibus at. Aliquam lacus nisi, sollicitudin at nisi nec, fermentum
            congue felis. Quisque mauris dolor, fringilla sed tincidunt ac,
            finibus non odio. Sed vitae mauris nec ante pretium finibus. Donec
            nisl neque, pharetra ac elit eu, faucibus aliquam ligula. Nullam
            dictum, tellus tincidunt tempor laoreet, nibh elit sollicitudin
            felis, eget.
          </p>

          <div className={style.gallery}>
            <img src={img1} alt="img" />
            <img src={img2} alt="img" />
            <img src={img3} alt="img" />
          </div>

          <p>
            Nam pulvinar blandit velit, id condimentum diam faucibus at. Aliquam
            lacus nisi, sollicitudin at nisi nec, fermentum congue felis.
            Quisque mauris dolor, fringilla sed tincidunt ac, finibus non odio.
            Sed vitae mauris nec ante pretium finibus. Donec nisl neque,
            pharetra ac elit eu, faucibus aliquam ligula. Nullam dictum, tellus
            tincidunt tempor laoreet, nibh elit sollicitudin felis, eget feugiat
            sapien diam nec nisl. Aenean gravida turpis nisi, consequat dictum
            risus dapibus a. Duis felis ante, varius in neque eu, tempor
            suscipit sem. Maecenas ullamcorper gravida sem sit amet cursus.
            Etiam pulvinar purus vitae justo pharetra consequat. Mauris id mi ut
            arcu feugiat maximus. Mauris consequat tellus id tempus aliquet.
          </p>
        </PanelContentWrapper>
      </Panel>
      <Panel
        title="What’s the minimum spec required to play HYENAS?"
        active={true}
      >
        <PanelContentWrapper>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu
            turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus
            nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum
            tellus elit sed risus. Maecenas eget condimentum velit, sit amet
            feugiat lectus. Class aptent taciti sociosqu ad litora torquent per
            conubia nostra, per inceptos himenaeos. Praesent auctor purus luctus
            enim egestas, ac scelerisque ante pulvinar. Donec ut rhoncus ex.
            Suspendisse ac rhoncus nisl, eu tempor urna. Curabitur vel bibendum
            lorem. Morbi convallis convallis diam sit amet lacinia. Aliquam in
            elementum tellus. Nam pulvinar blandit velit, id condimentum diam
            faucibus at. Aliquam lacus nisi, sollicitudin at nisi nec, fermentum
            congue felis. Quisque mauris dolor, fringilla sed tincidunt ac,
            finibus non odio. Sed vitae mauris nec ante pretium finibus. Donec
            nisl neque, pharetra ac elit eu, faucibus aliquam ligula. Nullam
            dictum, tellus tincidunt tempor laoreet, nibh elit sollicitudin
            felis, eget.
          </p>

          <div className={style.gallery}>
            <img src={img1} alt="img" />
            <img src={img2} alt="img" />
            <img src={img3} alt="img" />
          </div>

          <p>
            Nam pulvinar blandit velit, id condimentum diam faucibus at. Aliquam
            lacus nisi, sollicitudin at nisi nec, fermentum congue felis.
            Quisque mauris dolor, fringilla sed tincidunt ac, finibus non odio.
            Sed vitae mauris nec ante pretium finibus. Donec nisl neque,
            pharetra ac elit eu, faucibus aliquam ligula. Nullam dictum, tellus
            tincidunt tempor laoreet, nibh elit sollicitudin felis, eget feugiat
            sapien diam nec nisl. Aenean gravida turpis nisi, consequat dictum
            risus dapibus a. Duis felis ante, varius in neque eu, tempor
            suscipit sem. Maecenas ullamcorper gravida sem sit amet cursus.
            Etiam pulvinar purus vitae justo pharetra consequat. Mauris id mi ut
            arcu feugiat maximus. Mauris consequat tellus id tempus aliquet.
          </p>
        </PanelContentWrapper>
      </Panel>
      <Panel title="Where is the alpha available?">
        <PanelContentWrapper>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu
            turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus
            nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum
            tellus elit sed risus. Maecenas eget condimentum velit, sit amet
            feugiat lectus. Class aptent taciti sociosqu ad litora torquent per
            conubia nostra, per inceptos himenaeos. Praesent auctor purus luctus
            enim egestas, ac scelerisque ante pulvinar. Donec ut rhoncus ex.
            Suspendisse ac rhoncus nisl, eu tempor urna. Curabitur vel bibendum
            lorem. Morbi convallis convallis diam sit amet lacinia. Aliquam in
            elementum tellus. Nam pulvinar blandit velit, id condimentum diam
            faucibus at. Aliquam lacus nisi, sollicitudin at nisi nec, fermentum
            congue felis. Quisque mauris dolor, fringilla sed tincidunt ac,
            finibus non odio. Sed vitae mauris nec ante pretium finibus. Donec
            nisl neque, pharetra ac elit eu, faucibus aliquam ligula. Nullam
            dictum, tellus tincidunt tempor laoreet, nibh elit sollicitudin
            felis, eget.
          </p>

          <div className={style.gallery}>
            <img src={img1} alt="img" />
            <img src={img2} alt="img" />
            <img src={img3} alt="img" />
          </div>

          <p>
            Nam pulvinar blandit velit, id condimentum diam faucibus at. Aliquam
            lacus nisi, sollicitudin at nisi nec, fermentum congue felis.
            Quisque mauris dolor, fringilla sed tincidunt ac, finibus non odio.
            Sed vitae mauris nec ante pretium finibus. Donec nisl neque,
            pharetra ac elit eu, faucibus aliquam ligula. Nullam dictum, tellus
            tincidunt tempor laoreet, nibh elit sollicitudin felis, eget feugiat
            sapien diam nec nisl. Aenean gravida turpis nisi, consequat dictum
            risus dapibus a. Duis felis ante, varius in neque eu, tempor
            suscipit sem. Maecenas ullamcorper gravida sem sit amet cursus.
            Etiam pulvinar purus vitae justo pharetra consequat. Mauris id mi ut
            arcu feugiat maximus. Mauris consequat tellus id tempus aliquet.
          </p>
        </PanelContentWrapper>
      </Panel>
      <Panel title="Which platforms is the alpha available on?">
        <PanelContentWrapper>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu
            turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus
            nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum
            tellus elit sed risus. Maecenas eget condimentum velit, sit amet
            feugiat lectus. Class aptent taciti sociosqu ad litora torquent per
            conubia nostra, per inceptos himenaeos. Praesent auctor purus luctus
            enim egestas, ac scelerisque ante pulvinar. Donec ut rhoncus ex.
            Suspendisse ac rhoncus nisl, eu tempor urna. Curabitur vel bibendum
            lorem. Morbi convallis convallis diam sit amet lacinia. Aliquam in
            elementum tellus. Nam pulvinar blandit velit, id condimentum diam
            faucibus at. Aliquam lacus nisi, sollicitudin at nisi nec, fermentum
            congue felis. Quisque mauris dolor, fringilla sed tincidunt ac,
            finibus non odio. Sed vitae mauris nec ante pretium finibus. Donec
            nisl neque, pharetra ac elit eu, faucibus aliquam ligula. Nullam
            dictum, tellus tincidunt tempor laoreet, nibh elit sollicitudin
            felis, eget.
          </p>

          <div className={style.gallery}>
            <img src={img1} alt="img" />
            <img src={img2} alt="img" />
            <img src={img3} alt="img" />
          </div>

          <p>
            Nam pulvinar blandit velit, id condimentum diam faucibus at. Aliquam
            lacus nisi, sollicitudin at nisi nec, fermentum congue felis.
            Quisque mauris dolor, fringilla sed tincidunt ac, finibus non odio.
            Sed vitae mauris nec ante pretium finibus. Donec nisl neque,
            pharetra ac elit eu, faucibus aliquam ligula. Nullam dictum, tellus
            tincidunt tempor laoreet, nibh elit sollicitudin felis, eget feugiat
            sapien diam nec nisl. Aenean gravida turpis nisi, consequat dictum
            risus dapibus a. Duis felis ante, varius in neque eu, tempor
            suscipit sem. Maecenas ullamcorper gravida sem sit amet cursus.
            Etiam pulvinar purus vitae justo pharetra consequat. Mauris id mi ut
            arcu feugiat maximus. Mauris consequat tellus id tempus aliquet.
          </p>
        </PanelContentWrapper>
      </Panel>
      <Panel title="When can i play the full version?">
        <PanelContentWrapper>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu
            turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus
            nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum
            tellus elit sed risus. Maecenas eget condimentum velit, sit amet
            feugiat lectus. Class aptent taciti sociosqu ad litora torquent per
            conubia nostra, per inceptos himenaeos. Praesent auctor purus luctus
            enim egestas, ac scelerisque ante pulvinar. Donec ut rhoncus ex.
            Suspendisse ac rhoncus nisl, eu tempor urna. Curabitur vel bibendum
            lorem. Morbi convallis convallis diam sit amet lacinia. Aliquam in
            elementum tellus. Nam pulvinar blandit velit, id condimentum diam
            faucibus at. Aliquam lacus nisi, sollicitudin at nisi nec, fermentum
            congue felis. Quisque mauris dolor, fringilla sed tincidunt ac,
            finibus non odio. Sed vitae mauris nec ante pretium finibus. Donec
            nisl neque, pharetra ac elit eu, faucibus aliquam ligula. Nullam
            dictum, tellus tincidunt tempor laoreet, nibh elit sollicitudin
            felis, eget.
          </p>

          <div className={style.gallery}>
            <img src={img1} alt="img" />
            <img src={img2} alt="img" />
            <img src={img3} alt="img" />
          </div>

          <p>
            Nam pulvinar blandit velit, id condimentum diam faucibus at. Aliquam
            lacus nisi, sollicitudin at nisi nec, fermentum congue felis.
            Quisque mauris dolor, fringilla sed tincidunt ac, finibus non odio.
            Sed vitae mauris nec ante pretium finibus. Donec nisl neque,
            pharetra ac elit eu, faucibus aliquam ligula. Nullam dictum, tellus
            tincidunt tempor laoreet, nibh elit sollicitudin felis, eget feugiat
            sapien diam nec nisl. Aenean gravida turpis nisi, consequat dictum
            risus dapibus a. Duis felis ante, varius in neque eu, tempor
            suscipit sem. Maecenas ullamcorper gravida sem sit amet cursus.
            Etiam pulvinar purus vitae justo pharetra consequat. Mauris id mi ut
            arcu feugiat maximus. Mauris consequat tellus id tempus aliquet.
          </p>
        </PanelContentWrapper>
      </Panel>

      <button className={style.faq_load_more_btn}>
        <span>Load More</span>
      </button>
    </div>
  );
};

export default FaqAccordion;
