import * as style from "./MostFaq.module.css";

const question = [
  {
    id: 1,
    title: "Most frequently asked questions",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus.",
  },
  {
    id: 2,
    title: "Who can sign up?",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget.",
  },
  {
    id: 3,
    title: "When can i sign up?",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget.",
  },
];

const Question = ({ title, content }) => {
  return (
    <div className={style.box}>
      <h5>{title}</h5>
      <p>{content}</p>
    </div>
  );
};

const MostFaq = () => {
  return (
    <div className={style.most_faq}>
      <h2>
        <span>Most frequently asked</span> questions
      </h2>
      <div className={style.boxes}>
        {question.map((question) => {
          return (
            <Question
              key={question.id}
              title={question.title}
              content={question.content}
            />
          );
        })}
      </div>
    </div>
  );
};

export default MostFaq;
