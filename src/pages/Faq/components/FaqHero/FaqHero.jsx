import * as style from "./FaqHero.module.css";
import FrequentlyImg from "../FrequentlyImg/FrequentlyImg";
import Search from "./components/Search/Search";

const FaqHero = () => {
  return (
    <div className={style.faq_hero}>
      <div className={style.title}>
        {/* Mobile START */}
        <div className={style.mobile_only}>
          <FrequentlyImg mobileOnly={true} />
          <FrequentlyImg mobileOnly={true} />
          <FrequentlyImg mobileOnly={true} />
        </div>
        {/* Mobile END */}

        <div className={style.top}>
          <FrequentlyImg />
          <span>Frequently</span>
          <FrequentlyImg positionRight={true} />
        </div>
        <div className={style.bottom}>
          <h1>Asked questions</h1>
        </div>

        <div className={style.subtitle}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum
          dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit
          amet, consectetur adipiscing elit. mattis tellus.
        </div>

        <Search />
      </div>
    </div>
  );
};

export default FaqHero;
