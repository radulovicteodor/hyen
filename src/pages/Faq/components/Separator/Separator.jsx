import * as style from "./Separator.module.css";
import { motion } from "framer-motion";

const Separator = () => {
  return (
    <div className={style.separator}>
      <motion.div
        initial={{ x: "0vw" }}
        animate={{ x: ["-100vw", "100vw"] }}
        transition={{ repeat: Infinity, duration: 5 }}
      ></motion.div>
    </div>
  );
};

export default Separator;
