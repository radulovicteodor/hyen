import { useState, createContext } from "react";
import mediaItems from "../dummy.json";

export const MediaContext = createContext();

export const MediaProvider = ({ children }) => {
  const [data, setData] = useState(mediaItems);
  const [modalOpen, setModalOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState({});
  const [activeLink, setActiveLink] = useState("all");

  return (
    <MediaContext.Provider
      value={{
        data,
        setData,
        modalOpen,
        setModalOpen,
        selectedItem,
        setSelectedItem,
        activeLink,
        setActiveLink,
      }}
    >
      {children}
    </MediaContext.Provider>
  );
};

export default MediaContext;
