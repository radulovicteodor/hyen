import "./Media.css";
import Gallery from "./components/Gallery/Gallery";
import Hero from "./components/Hero/Hero";
import { MediaProvider } from "./context/MediaContext";

const Media = () => {
  return (
    <div className="media_section">
      <MediaProvider>
        <Hero />
        <Gallery />
      </MediaProvider>
    </div>
  );
};

export default Media;
