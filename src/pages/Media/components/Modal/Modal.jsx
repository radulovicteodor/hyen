/* eslint-disable react/prop-types */
import { motion } from "framer-motion";
import close from "../../../../assets/images/icons/close.svg";
import "./Modal.css";
import Backdrop from "./componenets/Backdrop";
import wallpaper from "../../../../assets/images/icons/wallpaper.svg";
import video from "../../../../assets/images/icons/video.svg";
import art from "../../../../assets/images/icons/art.svg";
import { useContext, useEffect } from "react";
import MediaContext from "../../context/MediaContext";

const dropIn = {
  hidden: {
    x: "100vw",
    opacity: 0,
  },
  visible: {
    x: "0",
    opacity: 1,
    transition: {
      duration: 5,
      type: "spring",
      damping: 20,
      stiffness: 200,
    },
  },
  exit: {
    x: "100vw",
    opacity: 0,
  },
};

const imgSrc = {
  video: video,
  artwork: art,
  wallpaper: wallpaper,
};

const Modal = ({ handleClose }) => {
  const { data, selectedItem, setSelectedItem } = useContext(MediaContext);
  const { id: selectedId, url: imageUrl, format, size, type } = selectedItem;

  const dataItems = data.items;

  const escKeyCode = 27;
  const arrowLeftKeyCode = 37;
  const arrowRightKeyCode = 39;

  const index = dataItems.findIndex((item) => item.id === selectedId);

  const nextImage = () => {
    if (index >= dataItems.length - 1) {
      setSelectedItem(dataItems[0]);
    } else {
      setSelectedItem(dataItems[index + 1]);
    }
  };

  const prevImage = () => {
    if (index === 0) {
      setSelectedItem(dataItems[dataItems.length - 1]);
    } else {
      setSelectedItem(dataItems[index - 1]);
    }
  };

  useEffect(() => {
    const handleKeyDown = (event) => {
      // ESC
      if (event.keyCode === escKeyCode) {
        handleClose();
      }

      // Right
      if (event.keyCode === arrowRightKeyCode) {
        nextImage();
      }
      // LEFT
      if (event.keyCode === arrowLeftKeyCode) {
        prevImage();
      }
    };

    window.addEventListener("keydown", handleKeyDown);

    return () => {
      window.removeEventListener("keydown", handleKeyDown);
    };
  }, [handleClose]);

  return (
    <Backdrop onClick={handleClose}>
      <motion.div
        onClick={(e) => e.stopPropagation()}
        className="modal orange-gradient"
        variants={dropIn}
        initial="hidden"
        animate="visible"
        exit="exit"
      >
        <div className="modal_content">
          <div className="media_asset">
            <img src={imageUrl} alt="media" />
          </div>

          <div className="modal_footer">
            <div className="left">
              <img src={imgSrc[type]} alt="type" />
              <p>{type}</p>
            </div>
            <div className="right">
              <p className="format">
                Format: <span>{format}</span>
              </p>
              |<p className="size">Size: {size}mb</p>
              <div className="download_btn">
                <span>Download</span>
              </div>
            </div>
          </div>
        </div>

        <img
          className="modal_close_icon"
          src={close}
          alt="close"
          onClick={handleClose}
        />
      </motion.div>
    </Backdrop>
  );
};

export default Modal;
