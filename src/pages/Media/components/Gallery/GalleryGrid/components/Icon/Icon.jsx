/* eslint-disable react/prop-types */
import "./Icon.css";
import wallpaper from "../../../../../../../assets/images/icons/wallpaper.svg";
import video from "../../../../../../../assets/images/icons/video.svg";
import art from "../../../../../../../assets/images/icons/art.svg";
import { motion } from "framer-motion";

const Icon = ({ type }) => {
  const imgSrc = {
    video: video,
    artwork: art,
    wallpaper: wallpaper,
  };

  return (
    <motion.div
      className="media_icon"
      initial={{ x: -30, y: 30, opacity: 0 }}
      animate={{ x: 0, y: 0, opacity: 1 }}
      transition={{ delay: 0.5, duration: 0.5 }}
    >
      <div className="media_image_tag"></div>
      <motion.img
        src={imgSrc[type]}
        alt="icon"
        style={{ zIndex: 2 }}
        initial={{ rotate: 45, y: 30, x: -10 }}
        animate={{ rotate: 0, y: 0, x: 0 }}
        transition={{ delay: 0.7, duration: 0.5, type: "spring" }}
      />
    </motion.div>
  );
};

export default Icon;
