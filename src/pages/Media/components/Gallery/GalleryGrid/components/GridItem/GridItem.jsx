/* eslint-disable react/prop-types */
import { useContext } from "react";
import Icon from "../Icon/Icon";
import "./GridItem.css";
import { motion } from "framer-motion";
import MediaContext from "../../../../../context/MediaContext";

const GridItem = ({ index, classExtension, item }) => {
  const variants = {
    hidden: { scale: 0, opacity: 0, x: "50vw" },
    animate: {
      scale: 1,
      x: 0,
      opacity: 1,
      transition: {
        duration: 0.1,
        delay: 0.05 * (index + 1),
      },
    },
  };

  const { setSelectedItem, setModalOpen } = useContext(MediaContext);

  return (
    <motion.div
      className={`grid_item grid_item${classExtension}`}
      onClick={() => {
        setModalOpen(true);
        const htmlEl = document.querySelector("html");
        htmlEl.classList.add("overflow_h");
        setSelectedItem(item);
      }}
      initial="hidden"
      animate="animate"
      variants={variants}
      key={item.id}
    >
      <div className="border"></div>
      <img src={item.url} alt={item.id} className="bg_image" />
      <Icon type={item.type} />
    </motion.div>
  );
};

export default GridItem;
