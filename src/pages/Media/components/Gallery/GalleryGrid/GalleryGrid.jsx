import "./GalleryGrid.css";
import GridItem from "./components/GridItem/GridItem";
import { motion } from "framer-motion";
import { useContext, useRef } from "react";
import MediaContext from "../../../context/MediaContext";

const GalleryGrid = () => {
  const classExtension = useRef(0);
  const galleryItemsLayoutNumber = 11;

  const { data, activeLink } = useContext(MediaContext);

  const filterMediaItems = data.items.filter((item) => {
    classExtension.current = 0;
    return item.type === activeLink || activeLink === "all";
  });

  return (
    <div className="gallery_grid">
      <motion.div className="grid_container" key={activeLink}>
        {filterMediaItems.map((item, index) => {
          if (classExtension.current === galleryItemsLayoutNumber) {
            classExtension.current = 1;
          } else {
            classExtension.current++;
          }

          return (
            <GridItem
              index={index}
              item={item}
              key={item.id}
              classExtension={classExtension.current}
            />
          );
        })}
      </motion.div>
    </div>
  );
};

export default GalleryGrid;
