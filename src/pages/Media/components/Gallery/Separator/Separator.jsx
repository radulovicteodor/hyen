import "./Separator.css";
import { motion } from "framer-motion";

const Separator = () => {
  return (
    <div className="gallery_separator">
      <motion.div
        initial={{ x: -172 }}
        animate={{ x: ["-100vw", "100vw"] }}
        transition={{ repeat: Infinity, duration: 5 }}
      ></motion.div>
    </div>
  );
};

export default Separator;
