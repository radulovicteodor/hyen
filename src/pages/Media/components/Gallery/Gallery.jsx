import { useContext } from "react";
import "./Gallery.css";
import GalleryGrid from "./GalleryGrid/GalleryGrid";
import Navigation from "./Navigation/Navigation";
import Separator from "./Separator/Separator";
import MediaContext from "../../context/MediaContext";
import { AnimatePresence } from "framer-motion";
import Modal from "../Modal/Modal";

const Gallery = () => {
  const { modalOpen, setModalOpen, selectedItem } = useContext(MediaContext);

  return (
    <div className="gallery_section">
      <Separator />
      <Navigation />
      <GalleryGrid />
      <AnimatePresence initial={false}>
        {modalOpen && (
          <Modal
            modalOpen={modalOpen}
            handleClose={() => {
              setModalOpen(false);
              const htmlEl = document.querySelector("html");
              htmlEl.classList.remove("overflow_h");
            }}
            selectedItem={selectedItem}
          />
        )}
      </AnimatePresence>
    </div>
  );
};

export default Gallery;
