import { motion, LayoutGroup } from "framer-motion";
import "./Navigation.css";
import { useContext, useRef } from "react";
import MediaContext from "../../../context/MediaContext";

const Navigation = () => {
  const links = [
    { id: "all", title: "ALL" },
    { id: "wallpaper", title: "WALLPAPERS" },
    { id: "video", title: "VIDEOS" },
    { id: "artwork", title: "ARTWORK" },
    { id: "logos", title: "LOGOS" },
  ];

  const menuRef = useRef(null);

  const onLinkClick = (e) => {
    const mobileLayout = 1024;
    if (window.innerWidth > mobileLayout) return;

    const element = e.target;
    const elementRect = element.getBoundingClientRect();
    const menuRect = menuRef.current.getBoundingClientRect();

    if (elementRect.left < menuRect.left) {
      menuRef.current.scrollBy({
        left: elementRect.left - menuRect.left - 20,
        behavior: "smooth",
      });
    } else if (elementRect.right > window.innerWidth) {
      menuRef.current.scrollBy({
        left: elementRect.right - window.innerWidth + 20,
        behavior: "smooth",
      });
    }
  };

  const { activeLink, setActiveLink } = useContext(MediaContext);

  return (
    <div ref={menuRef} className="gallery_navigation">
      <LayoutGroup>
        {links.map((link) => (
          <motion.div
            key={link.id}
            layoutId={link.id}
            className={
              activeLink === link.id
                ? "gallery_navigation_link active"
                : "gallery_navigation_link"
            }
            onClick={(e) => {
              setActiveLink(link.id);
              onLinkClick(e);
            }}
          >
            <a href={link.path} style={{ zIndex: 3 }}>
              {link.title}
            </a>
            {activeLink === link.id && (
              <motion.div
                className="navigation_background"
                layoutId="background"
                transition={{ type: "spring", damping: 14, duration: 20 }}
                style={{ border: "5px solid white" }}
              />
            )}
          </motion.div>
        ))}
      </LayoutGroup>

      {/* <AnimatePresence>
        {activeLink && (
          <motion.div
            className="background"
            layoutId="background"
          
          />
        )}
      </AnimatePresence> */}
    </div>
  );
};

export default Navigation;
