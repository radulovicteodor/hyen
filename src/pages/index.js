export { default as Media } from "./Media/Media";
export { default as News } from "./News/News";
export { default as NewsArticle } from "./NewsArticle/NewsArticle";
export { default as Faq } from "./Faq/Faq";
