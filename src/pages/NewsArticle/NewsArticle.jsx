import {
  ArticleHeader,
  ArticleImage,
  ArticleShare,
  ArticleSlider,
  ArticleText,
  RelatedArticles,
} from "./components";
import * as style from "./newsArticle.module.css";

import img3 from "../../assets/images/newsArticle/img3.jpg";
import img4 from "../../assets/images/newsArticle/img4.jpg";

const NewsArticle = () => {
  return (
    <div className={style.news_article}>
      <ArticleHeader />;
      <ArticleSlider />
      <ArticleText>
        <p>
          {`Hyenas remains one of the most interesting new games for 2023. It's
          coming from Creative Assembly, the studio best known for creating some
          of the best strategy games of all time –and for one of the best horror
          games of the last decade. But its Hyenas game is something entirely
          new, a competitive multiplayer game that's set to challenge the likes
          of Overwatch 2.`}
        </p>
      </ArticleText>
      <ArticleText>
        <p>
          {`With its slick visual style and kinetic, gravity-defying combat,
          Hyenas is something we're keen to see more of. It's undoubtedly one of
          the most anticipated upcoming PC games for the new year, although
          Creative Assembly has confirmed that it'll be coming to console too.
          While we wait for the first Hyenas gameplay footage, why not read up
          on everything we know about the game so far.`}
        </p>
      </ArticleText>
      <ArticleImage
        src={img3}
        border={true}
        alt="Image caption short description"
      />
      <ArticleText>
        <h2>
          “Lorem ipsum dolor sit <span>amet, consectetur</span> adipiscing
          elit.”
        </h2>
      </ArticleText>
      <ArticleText>
        <h6>John Doe - Game Analyst, IGN games</h6>
      </ArticleText>
      <ArticleImage src={img4} />
      <ArticleText>
        <p>
          {`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit amet feugiat lectus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent auctor purus luctus enim egestas, ac scelerisque ante pulvinar. Donec ut rhoncus ex. Suspendisse ac rhoncus nisl, eu tempor urna. Curabitur vel bibendum lorem. Morbi convallis convallis diam sit amet lacinia. Aliquam in elementum tellus.`}
        </p>
      </ArticleText>
      <ArticleText>
        <p>
          {`Curabitur tempor quis eros tempus lacinia. Nam bibendum pellentesque quam a convallis. Sed ut vulputate nisi. Integer in felis sed leo vestibulum venenatis. Suspendisse quis arcu sem. Aenean feugiat ex eu vestibulum vestibulum. Morbi a eleifend magna. Nam metus lacus, porttitor eu mauris a, blandit ultrices nibh. Mauris sit amet magna non ligula vestibulum eleifend. Nulla varius volutpat turpis sed lacinia. Nam eget mi in purus lobortis eleifend. Sed nec ante dictum sem condimentum ullamcorper quis venenatis nisi. Proin vitae facilisis nisi, ac posuere leo.`}
        </p>
      </ArticleText>
      <ArticleText>
        <p>
          {`Nam pulvinar blandit velit, id condimentum diam faucibus at. Aliquam lacus nisi, sollicitudin at nisi nec, fermentum congue felis. Quisque mauris dolor, fringilla sed tincidunt ac, finibus non odio. Sed vitae mauris nec ante pretium finibus. Donec nisl neque, pharetra ac elit eu, faucibus aliquam ligula. Nullam dictum, tellus tincidunt tempor laoreet, nibh elit sollicitudin felis, eget feugiat sapien diam nec nisl. Aenean gravida turpis nisi, consequat dictum risus dapibus a. Duis felis ante, varius in neque eu, tempor suscipit sem. Maecenas ullamcorper gravida sem sit amet cursus. Etiam pulvinar purus vitae justo pharetra consequat. Mauris id mi ut arcu feugiat maximus. Mauris consequat tellus id tempus aliquet.`}
        </p>
      </ArticleText>
      <ArticleShare />
      <RelatedArticles />
    </div>
  );
};

export default NewsArticle;
