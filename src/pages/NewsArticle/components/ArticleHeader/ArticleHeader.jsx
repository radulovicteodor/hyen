import { NavLink } from "react-router-dom";
import { BackIcon, Facebook, Reddit, Twitter, Link } from "../../icons";
import * as style from "./articleHeader.module.css";
import bgImage from "/img/newsArticle/artcile_bg.jpg";

const ArticleHeader = () => {
  return (
    <>
      <div
        className={style.article_header}
        style={{ background: `url(${bgImage}) no-repeat center/cover` }}
      >
        <div className={style.header_overlay}></div>
        <NavLink
          to={{
            pathname: "/news",
          }}
        >
          <div className={style.back_btn}>
            <BackIcon />
            <span>Back to Article</span>
          </div>
        </NavLink>
        <div className={style.category}>
          <span>Category</span>
        </div>
      </div>
      <div className={style.article_details}>
        <div className={style.container}>
          <h1>{`Hyenas: Everything we know about Creative Assembly's new FPS`}</h1>
          <h6>
            {` The studio behind Total War and Alien Isolation is veering into the
            world of hero shooters with Hyenas, an upcoming free-to-play
            first-person shooter that's set to rock 2023`}
          </h6>
          <div className={style.news_share}>
            <div className={style.date}>23 feb 2023</div>
            <div className={style.share}>
              SHARE
              <a href="#">
                <Twitter />
              </a>
              <a href="#">
                <Facebook />
              </a>
              <a href="#">
                <Reddit />
              </a>
              <a href="#">
                <Link />
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ArticleHeader;
