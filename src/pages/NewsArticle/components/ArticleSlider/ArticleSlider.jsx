import { Splide, SplideSlide } from "@splidejs/react-splide";
import "@splidejs/splide/dist/css/themes/splide-default.min.css";

import "./articleSlider.css";

import img1 from "../../../../assets/images/newsArticle/img1.png";
import img2 from "../../../../assets/images/newsArticle/img2.jpg";
import img3 from "../../../../assets/images/newsArticle/img3.jpg";
import { useEffect, useRef } from "react";

const images = [img1, img2, img3];

const mainOptions = {
  type: "fade",
  fixedHeight: 700,
  pagination: false,
  arrows: false,
  cover: true,

  breakpoints: {
    1024: {
      pagination: true,
      arrows: true,
      fixedHeight: 350,
    },
  },
};

const thumbsOptions = {
  rewind: true,
  fixedWidth: 88,
  fixedHeight: 88,
  isNavigation: true,
  gap: 16,
  pagination: false,
  cover: true,
  arrows: false,
  hidding: true,
  dragMinThreshold: {
    mouse: 4,
    touch: 10,
  },
};
const ArticleSlider = () => {
  const mainRef = useRef(null);
  const thumbsRef = useRef(null);

  useEffect(() => {
    if (mainRef.current && thumbsRef.current && thumbsRef.current.splide) {
      mainRef.current.sync(thumbsRef.current.splide);
    }
  }, [mainRef, thumbsRef]);
  return (
    <div className="carousel_container">
      {/* Main */}
      <Splide ref={mainRef} options={mainOptions} aria-label="news_slider">
        {images.map((img, index) => {
          return (
            <SplideSlide key={index}>
              <img src={img} alt="Image 1" />
            </SplideSlide>
          );
        })}
      </Splide>

      {/* Thumbs */}
      <Splide
        className="thumbs_layout"
        ref={thumbsRef}
        options={thumbsOptions}
        aria-label="My Favorite Images"
      >
        {images.map((img, index) => {
          return (
            <SplideSlide key={index}>
              <img src={img} alt="Image 1" />
            </SplideSlide>
          );
        })}
      </Splide>
    </div>
  );
};

export default ArticleSlider;
