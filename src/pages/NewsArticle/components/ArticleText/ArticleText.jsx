import * as style from "./text.module.css";

const ArticleText = (props) => {
  return <div className={style.text}>{props.children}</div>;
};

export default ArticleText;
