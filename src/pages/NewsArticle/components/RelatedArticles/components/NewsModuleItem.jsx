import { NavLink } from "react-router-dom";

import cn from "classnames";
import * as style from "./newsModule.module.css";

const NewsModuleItem = ({
  itemId,
  title,
  category = "cyan",
  children,
  imgSrc,
}) => {
  return (
    <NavLink
      to={{
        pathname: `/news/${itemId}`,
      }}
      className={style.item}
    >
      <div className={style.image}>
        <img src={imgSrc} alt="shbbkbb" />
      </div>
      <div className={style.gradient} />
      <div className={style.content}>
        <div className={style.contentBackground} />
        <div className={cn(style.categoryPill, category)}>{category}</div>
        <h5>{title}</h5>
        <div className={style.contentText}>{children}</div>
        <h6>23 Feb 2023</h6>
      </div>
    </NavLink>
  );
};

export default NewsModuleItem;
