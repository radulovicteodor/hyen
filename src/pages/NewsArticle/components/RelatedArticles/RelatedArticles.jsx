import NewsModuleItem from "./components/NewsModuleItem";
import * as style from "./relatedArticles.module.css";

import news1 from "/img/news/Img (1).png";
import news2 from "/img/news/Img (2).png";
import news3 from "/img/news/Img.png";
import news4 from "/img/news/Rectangle 15.png";

const RelatedArticles = () => {
  return (
    <div className={style.related_articles}>
      <div className={style.title}>Related Articles</div>
      <div className={style.container}>
        <NewsModuleItem
          itemId={0}
          category="cian"
          title="Lorem ipsum dolor sit amet, adipiscing ud elit."
          imgSrc={news1}
        >
          <p>
            Nam bibendum pellentesque quam a convallis. Sed ut vulputate nisi.
            Integer in felis sed leo vestibulum (...)
          </p>
          <p>
            Sources, who recently played the game as a part of its recent alpha
            playtest have said that the mode is similar to Escape From Tarkov’s
            offline Raid mode or Payday. “The objective is to get in [the map],
            loot merch, and get out”, said one player.
          </p>
        </NewsModuleItem>
        <NewsModuleItem
          itemId={1}
          category="yellow"
          title="Feel the love with
          the Ultimate
          Valentine event!"
          imgSrc={news2}
        >
          <p>
            Aenean feugiat ex eu vestibulum vestibulum. Morbi a eleifend magna.
          </p>
          <p>
            Sources, who recently played the game as a part of its recent alpha
            playtest have said that the mode is similar to Escape From Tarkov’s
            offline Raid mode or Payday. “The objective is to get in [the map],
            loot merch, and get out”, said one player.
          </p>
        </NewsModuleItem>
        <NewsModuleItem
          itemId={2}
          category="yellow"
          title="Feel the love with
          the Ultimate
          Valentine event!"
          imgSrc={news3}
        >
          <p>
            Mauris sit amet magna non ligula vestibulum eleifend. Nulla varius
            volutpat turpis sed lacinia.
          </p>
          <p>
            Sources, who recently played the game as a part of its recent alpha
            playtest have said that the mode is similar to Escape From Tarkov’s
            offline Raid mode or Payday. “The objective is to get in [the map],
            loot merch, and get out”, said one player.
          </p>
        </NewsModuleItem>
        <NewsModuleItem
          itemId={3}
          category="pink"
          title="Feel the love with
          the Ultimate
          Valentine event!"
          imgSrc={news4}
        >
          <p>
            Pulvinar blandit velit, id condimentum diam faucibus at. Aliquam
            lacus nisi, sollicitus.
          </p>
          <p>
            Sources, who recently played the game as a part of its recent alpha
            playtest have said that the mode is similar to Escape From Tarkov’s
            offline Raid mode or Payday. “The objective is to get in [the map],
            loot merch, and get out”, said one player.
          </p>
        </NewsModuleItem>
      </div>

      <button className={style.related_articles_load_more_btn}>
        <span>All Articles</span>
      </button>
    </div>
  );
};

export default RelatedArticles;
