export { default as ArticleHeader } from "./ArticleHeader/ArticleHeader";
export { default as ArticleSlider } from "./ArticleSlider/ArticleSlider";
export { default as ArticleText } from "./ArticleText/ArticleText";
export { default as ArticleImage } from "./ArticleImage/ArticleImage";
export { default as ArticleShare } from "./ArticleShare/ArticleShare";
export { default as RelatedArticles } from "./RelatedArticles/RelatedArticles";
