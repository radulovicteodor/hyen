import * as style from "./articleShare.module.css";
import { Facebook, Reddit, Twitter, Link } from "../../icons";

const ArticleShare = () => {
  return (
    <div className={style.news_share}>
      <div className={style.share}>
        SHARE
        <a href="#">
          <Twitter />
        </a>
        <a href="#">
          <Facebook />
        </a>
        <a href="#">
          <Reddit />
        </a>
        <a href="#">
          <Link />
        </a>
      </div>
      <div className={style.line} />
    </div>
  );
};

export default ArticleShare;
