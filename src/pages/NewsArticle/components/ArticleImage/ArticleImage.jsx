import * as style from "./articleImage.module.css";

const ArticleImage = (props) => {
  return (
    <div className={style.article_image}>
      <div
        className={`${style.img_wrapper} ${
          props.border ? style.image_border : ""
        }`}
      >
        <img src={props.src} alt={props.name} />

        {props.alt && (
          <div className={style.image_alt}>
            <p>{props.alt}</p>
          </div>
        )}
      </div>
    </div>
  );
};

export default ArticleImage;
