import Hero from "./components/Hero/Hero";
import NewsContent from "./components/NewsContent/NewsContent";
import * as style from "./News.module.css";

const News = () => {
  return (
    <div className={style.news_container}>
      <Hero />
      <NewsContent />
    </div>
  );
};

export default News;
