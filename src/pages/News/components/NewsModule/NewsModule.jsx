/* eslint-disable react/no-unescaped-entities */
import cn from "classnames";

import * as style from "./newsModule.module.css";

import news1 from "/img/news/Img (1).png";
import news2 from "/img/news/Img (2).png";
import news3 from "/img/news/Img (3).png";
import news4 from "/img/news/Img (4).png";
import news5 from "/img/news/Img (5).png";
import news6 from "/img/news/Img (6).png";
import news7 from "/img/news/Img (7).png";
import news8 from "/img/news/Img (8).png";
import news9 from "/img/news/Img (9).png";
import news0 from "/img/news/Rectangle 15.png";
import Navigation from "../Navigation/Navigation";
import { NavLink } from "react-router-dom";

const newsItemsImages = [
  news0,
  news1,
  news2,
  news3,
  news4,
  news5,
  news6,
  news7,
  news8,
  news9,
];

const NewsModuleItem = ({ itemId, title, category = "cyan", children }) => {
  return (
    <NavLink
      to={{
        pathname: `/news/${itemId}`,
      }}
      className={cn(style.item, itemId === 0 && style.first)}
    >
      <div className={style.image}>
        <img src={newsItemsImages[itemId]} alt="shbbkbb" />
      </div>
      <div className={style.gradient} />
      <div className={style.content}>
        <div className={style.contentBackground} />
        <div className={cn(style.categoryPill, category)}>{category}</div>
        <h5>{title}</h5>
        <div className={style.contentText}>{children}</div>
        <h6>23 Feb 2023</h6>
      </div>
    </NavLink>
  );
};

const NewsModule = () => {
  return (
    <>
      <div className={style.container}>
        <NewsModuleItem
          itemId={0}
          category="yellow"
          title="Lorem ipsum dolor sit amet, coectetur adipiscing elit. "
        >
          <p>
            Creative Assembly has revealed a new PvE mode named Plunder Raid
            during its recent alpha playtests of Hyenas.
          </p>
          <p>
            Sources, who recently played the game as a part of its recent alpha
            playtest have said that the mode is similar to Escape From Tarkov’s
            offline Raid mode or Payday. “The objective is to get in [the map],
            loot merch, and get out”, said one player.
          </p>
        </NewsModuleItem>

        <Navigation />

        <NewsModuleItem
          itemId={1}
          category="yellow"
          title="Lorem ipsum dolor sit amet, coectetur adipiscing elit. "
        >
          <p>
            Creative Assembly has revealed a new PvE mode named Plunder Raid
            during its recent alpha playtests of Hyenas.
          </p>
          <p>
            Sources, who recently played the game as a part of its recent alpha
            playtest have said that the mode is similar to Escape From Tarkov’s
            offline Raid mode or Payday. “The objective is to get in [the map],
            loot merch, and get out”, said one player.
          </p>
        </NewsModuleItem>
        <NewsModuleItem
          itemId={2}
          category="pink"
          title="Lorem ipsum dolor sit amet, coectetur adipiscing elit. "
        >
          <p>
            The "Hyenas" tournament, which attracted thousands of viewers from
            around the world, was hailed as a huge success by organizers. They
            praised the players for their dedication, sportsmanship, and skill,
            saying that the tournament showcased the best of what online gaming
            has to offer.
          </p>
        </NewsModuleItem>
      </div>
      <div className={style.container}>
        <NewsModuleItem
          itemId={3}
          category="yellow"
          title="Lorem ipsum dolor sit amet, coectetur adipiscing elit. "
        >
          <p>
            HYENAS is a hero-based, multiplayer extraction shooter where you
            play as misfit anti-heroes competing against other squads and
            security forces to steal valuable merch with epic zero-G combat.
          </p>
        </NewsModuleItem>
        <NewsModuleItem
          itemId={4}
          category="yellow"
          title="Lorem ipsum dolor sit amet, coectetur adipiscing elit. "
        >
          <p>
            Creative Assembly has revealed a new PvE mode named Plunder Raid
            during its recent alpha playtests of Hyenas.
          </p>
          <p>
            Sources, who recently played the game as a part of its recent alpha
            playtest have said that the mode is similar to Escape From Tarkov’s
            offline Raid mode or Payday. “The objective is to get in [the map],
            loot merch, and get out”, said one player.
          </p>
        </NewsModuleItem>
        <NewsModuleItem
          itemId={5}
          category="pink"
          title="Lorem ipsum dolor sit amet, coectetur adipiscing elit. "
        >
          <p>
            The "Hyenas" tournament, which attracted thousands of viewers from
            around the world, was hailed as a huge success by organizers. They
            praised the players for their dedication, sportsmanship, and skill,
            saying that the tournament showcased the best of what online gaming
            has to offer.
          </p>
        </NewsModuleItem>
      </div>
      <div className={style.container}>
        <NewsModuleItem
          itemId={6}
          category="cyan"
          title="Lorem ipsum dolor sit amet, coectetur adipiscing elit. "
        >
          <p>
            Hyenas remains one of the most interesting new games for 2023. It's
            coming from Creative Assembly, the studio best known for creating
            some of the best strategy games of all time –and for one of the best
            horror games of the last decade.
          </p>
        </NewsModuleItem>
        <NewsModuleItem
          itemId={7}
          category="yellow"
          title="Lorem ipsum dolor sit amet, coectetur adipiscing elit. "
        >
          <p>
            HYENAS is a hero-based, multiplayer extraction shooter where you
            play as misfit anti-heroes competing against other squads and
            security forces to steal valuable merch with epic zero-G combat.
          </p>
        </NewsModuleItem>
        <NewsModuleItem
          itemId={8}
          category="yellow"
          title="Lorem ipsum dolor sit amet, coectetur adipiscing elit. "
        >
          <p>
            Creative Assembly has revealed a new PvE mode named Plunder Raid
            during its recent alpha playtests of Hyenas.
          </p>
          <p>
            Sources, who recently played the game as a part of its recent alpha
            playtest have said that the mode is similar to Escape From Tarkov’s
            offline Raid mode or Payday. “The objective is to get in [the map],
            loot merch, and get out”, said one player.
          </p>
        </NewsModuleItem>
        <NewsModuleItem
          itemId={9}
          category="pink"
          title="Lorem ipsum dolor sit amet, coectetur adipiscing elit. "
        >
          <p>
            The "Hyenas" tournament, which attracted thousands of viewers from
            around the world, was hailed as a huge success by organizers. They
            praised the players for their dedication, sportsmanship, and skill,
            saying that the tournament showcased the best of what online gaming
            has to offer.
          </p>
        </NewsModuleItem>
      </div>
      <button className={style.news_load_more_btn}>
        <span>Load More</span>
      </button>
    </>
  );
};

export default NewsModule;
