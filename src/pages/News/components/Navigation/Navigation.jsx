import { motion, LayoutGroup } from "framer-motion";
import { useRef, useState } from "react";
import * as style from "./Navigation.module.css";
import classNames from "classnames";

const Navigation = () => {
  const links = [
    { id: "all", title: "ALL CATEGORY" },
    { id: "CATEGORY1", title: "CATEGORY1" },
    { id: "CATEGORY2", title: "CATEGORY2" },
    { id: "CATEGORY3", title: "CATEGORY3" },
    { id: "CATEGORY4", title: "CATEGORY4" },
    { id: "CATEGORY5", title: "CATEGORY5" },
  ];

  const [activeLink, setActiveLink] = useState("all");

  const menuRef = useRef(null);

  const onLinkClick = (e) => {
    const mobileLayout = 1024;
    if (window.innerWidth > mobileLayout) return;

    const element = e.target;
    const elementRect = element.getBoundingClientRect();
    const menuRect = menuRef.current.getBoundingClientRect();

    if (elementRect.left < menuRect.left) {
      menuRef.current.scrollBy({
        left: elementRect.left - menuRect.left - 20,
        behavior: "smooth",
      });
    } else if (elementRect.right > window.innerWidth) {
      menuRef.current.scrollBy({
        left: elementRect.right - window.innerWidth + 20,
        behavior: "smooth",
      });
    }
  };

  return (
    <div ref={menuRef} className={style.navigation}>
      <motion.div
        style={{ display: "none" }}
        initial={{ x: 0 }}
        animate={{ x: ["-100vw", "100vw"] }}
        transition={{ repeat: Infinity, duration: 5 }}
      ></motion.div>
      <LayoutGroup>
        {links.map((link) => (
          <motion.div
            key={link.id}
            layoutId={link.id}
            className={classNames(
              style.navigation_link,
              activeLink === link.id && style.active
            )}
            onClick={(e) => {
              setActiveLink(link.id);
              onLinkClick(e);
            }}
          >
            <a href={link.path} style={{ zIndex: 3 }}>
              {link.title}
            </a>
            {activeLink === link.id && (
              <motion.div
                className={style.navigation_background}
                layoutId="background"
                transition={{ type: "spring", damping: 14, duration: 20 }}
                style={{ border: "5px solid rgba(78, 0, 255, 1)" }}
              />
            )}
          </motion.div>
        ))}
      </LayoutGroup>

      {/* <AnimatePresence>
        {activeLink && (
          <motion.div
            className="background"
            layoutId="background"
          
          />
        )}
      </AnimatePresence> */}
    </div>
  );
};

export default Navigation;
