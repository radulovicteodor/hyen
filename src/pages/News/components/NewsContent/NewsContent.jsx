import NewsModule from "../NewsModule/NewsModule";

import cn from "classnames";

import * as s from "../NewsModule/newsModule.module.css";
import * as style from "./NewsContent.module.css";
import { NavLink } from "react-router-dom";

const NewsContent = () => {
  return (
    <div className={style.news_content}>
      <NavLink
        to={{
          pathname: "/news/1",
        }}
      >
        <div className={style.head_news}>
          <div className={style.head_news_content}>
            <div className={cn(s.categoryPill, "category") + style.category}>
              {"category"}
            </div>

            <h5>
              Vorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc
              vulputate libero et velit.
            </h5>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu
              turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus
              nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum
              tellus elit sed risus. Maecenas eget condimentum velit, sit amet
              feugiat lectus. Class aptent taciti sociosqu ad litora torquent
              per conubia .
            </p>
            <span>23 Feb 2023</span>
          </div>
          <div className={style.head_news_image}>
            <img src="./img/news/Rectangle 15.png" />
          </div>
        </div>
      </NavLink>
      <NewsModule />
    </div>
  );
};

export default NewsContent;
