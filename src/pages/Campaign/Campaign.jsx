import * as style from "./Campaign.module.css";
import Benefits from "./components/Benefits/Benefits";
import Cards from "./components/Cards/Cards";
import Hero from "./components/Hero/Hero";
import SignUp from "./components/SignUp/SignUp";
import TopographicBackground from "./components/TopographicBackground/TopographicBackground";
import VPCards from "./components/VPCards/VPCards";

const Campaign = () => {
  return (
    <div className={style.campaign}>
      <Hero />
      <TopographicBackground>
        <VPCards />
        <Benefits />
        <Cards />
      </TopographicBackground>
      <SignUp />
    </div>
  );
};

export default Campaign;
