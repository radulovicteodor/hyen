import * as style from "./Benefits.module.css";
import { Parallax } from "react-scroll-parallax";

import heroImg from "../../../../assets/images/campaing/hero.svg";
import heroImgMob from "../../../../assets/images/campaing/hero_mobile.svg";
import bullet1 from "../../../../assets/images/campaing/bullet1.svg";
import bullet2 from "../../../../assets/images/campaing/bullet2.svg";

const Benefits = () => {
  return (
    <div className={style.benefits_container}>
      <div className={style.bullet_wrapper}>
        <div className={style.border_wrapper}>
          <div className={style.benefits}>
            <div className={style.title}>
              <h3>Benefits</h3>
            </div>

            <div className={style.content}>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                eu turpis molestie, dictum est a, mattis tellus. Sed dignissim,
                metus nec fringilla accumsan.
              </p>

              <div className={style.list}>
                <h5 className={style.list_item}>
                  Exclusive Previews and first-peeks
                </h5>
                <h5 className={style.list_item}>In-game cosmetics</h5>
                <h5 className={style.list_item}>Exclsuive news</h5>
                <h5 className={style.list_item}>Newsletters</h5>
              </div>

              <div className={style.button}>
                <span>Primary button</span>
              </div>
            </div>

            <div className={style.hero}>
              <img src={heroImg} alt="hero" />
              <img className={style.mobile_only} src={heroImgMob} alt="hero" />
            </div>
          </div>
        </div>
        <Parallax
          className={style.bullet_1}
          translateX={["-60%", "-60%"]}
          translateY={["-60%", "40%"]}
          scale={[0.3, 1]}
          rotate={[-180, 0]}
          easing="easeInQuad"
        >
          <img src={bullet1} alt="bullet1" />
        </Parallax>
        <Parallax
          className={style.bullet_2}
          translateX={["-20%", "-140%"]}
          scale={[0.4, 1]}
          rotate={[-180, 0]}
          easing="easeInQuad"
        >
          <img src={bullet2} alt="bullet2" />
        </Parallax>
      </div>
    </div>
  );
};

export default Benefits;
