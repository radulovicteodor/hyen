import * as style from "./Card.module.css";

const Card = ({ imgSrc, title, content }) => {
  return (
    <div className={style.card}>
      <div className={style.circle_image}>
        <div className={style.img_wrapper}>
          <img src={imgSrc} alt="image" />
        </div>
      </div>

      <div className={style.content}>
        <h4>{title}</h4>
        <p>{content}</p>
      </div>
    </div>
  );
};

export default Card;
