import * as style from "./VPCards.module.css";
import Card from "./Components/Card/Card";

import img1 from "../../../../assets/images/media/media1.png";
import img2 from "../../../../assets/images/media/media5.jpg";
import img3 from "../../../../assets/images/media/media2.jpg";

const VPCards = () => {
  return (
    <div className={style.vp_cards}>
      <Card
        key={1}
        imgSrc={img1}
        title="VP #1"
        content="Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet."
      />
      <Card
        key={2}
        imgSrc={img2}
        title="VP #2"
        content="Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet."
      />
      <Card
        key={3}
        imgSrc={img3}
        title="VP #3"
        content="Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet."
      />
    </div>
  );
};

export default VPCards;
