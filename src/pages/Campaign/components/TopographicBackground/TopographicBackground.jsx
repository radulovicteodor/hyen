import * as style from "./TopographicBackground.module.css";

import {
  Topographic1,
  Topographic2,
} from "./components/Topographic/Topographic";

const TopographicBackground = ({ children }) => {
  return (
    <div className={style.content_wrapper}>
      <Topographic1 />
      <Topographic2 />
      <div className={style.content}>{children}</div>
    </div>
  );
};

export default TopographicBackground;
