import * as style from "./HeroSeparator.module.css";

const HeroSeparator = () => {
  return (
    <div className={style.hero_separator}>
      <svg width="393" height="93" viewBox="0 0 393 93" fill="none">
        <mask
          id="mask0_670_28749"
          maskUnits="userSpaceOnUse"
          x="0"
          y="0"
          width="393"
          height="93"
          style={{ maskType: "alpha" }}
        >
          <rect width="393" height="93" x="0" y="0" fill="#D9D9D9"></rect>
        </mask>
        <g mask="url(#mask0_670_28749)">
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M933 90C933 102.703 922.703 113 910 113H-516C-528.703 113 -539 102.703 -539 90V73C-539 60.2975 -528.703 50 -516 50H5.94305C17.5892 50 26.9328 40.8361 32.8146 30.7844C41.4901 15.9583 57.582 6 76 6H319C337.418 6 353.51 15.9583 362.185 30.7844C368.067 40.8361 377.411 50 389.057 50H910C922.703 50 933 60.2975 933 73V90Z"
            fill="url(#paint0_linear_670_28749)"
          ></path>
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M980 93C980 105.703 969.703 116 957 116H-563C-575.703 116 -586 105.703 -586 93V76C-586 63.2975 -575.703 53 -563 53H6.94305C18.5892 53 27.9328 43.8361 33.8146 33.7844C42.4901 18.9583 58.582 9 77 9H318C336.418 9 352.51 18.9583 361.185 33.7844C367.067 43.8361 376.411 53 388.057 53H957C969.703 53 980 63.2975 980 76V93Z"
            fill="#050241"
          ></path>
        </g>
        <defs>
          <linearGradient
            id="paint0_linear_670_28749"
            x1="-1090.35"
            y1="125.483"
            x2="-342.501"
            y2="-912.753"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="#00FFDC"></stop>
            <stop offset="1" stopColor="#4E00FF"></stop>
          </linearGradient>
        </defs>
      </svg>
    </div>
  );
};

export default HeroSeparator;
