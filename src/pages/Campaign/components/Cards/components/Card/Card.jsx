import * as style from "./Card.module.css";

const Card = ({ title, content, bigCard, imgSrc, imgLeft }) => {
  return (
    <div
      className={`${bigCard ? style.card_big : style.card_small} ${
        imgLeft && bigCard ? style.reverse : ""
      }`}
    >
      <div className={style.text_container}>
        <h3>{title}</h3>
        <p>{content}</p>
      </div>
      <div className={style.img_container}>
        <img src={imgSrc} />
      </div>
    </div>
  );
};

export default Card;
