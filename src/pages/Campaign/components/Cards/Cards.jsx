import { Parallax } from "react-scroll-parallax";
import * as style from "./Cards.module.css";
import Card from "./components/Card/Card";

const Cards = () => {
  return (
    <div className={style.cards}>
      <Parallax
        className={style.wright}
        translateX={["-90%", "40%"]}
        easing="easeInQuad"
        style={{
          left: 0,
          z: 1,
        }}
      >
        <img src="./img/campaign/Wright.png" />
      </Parallax>
      <div className={style.cards_container}>
        <Card
          title="Lorem ipsum dolor sit amet consectetur et dolor cavis"
          content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu
          turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec
          fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus
          elit sed risus. Maecenas eget condimentum velit, sit amet feugiat
          lectus. Class aptent taciti sociosqu ad litora torquent per conubia
          nostra, per inceptos himenaeos. Praesent auctor purus luctus enim
          egestas."
          bigCard={true}
          imgSrc="./img/campaign/big_1.png"
        />
        <Card
          title="Lorem ipsum dolor sit amet consectetur et dolor cavis"
          content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu
          turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus
          nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum
          tellus elit sed risus. Maecenas eget condimentum velit, sit amet
          feugiat lectus. Class aptent taciti sociosqu ad litora torquent
          per conubia nostra, per inceptos himenaeos. Praesent auctor purus
          luctus enim egestas."
          bigCard={true}
          imgLeft={true}
          imgSrc="./img/campaign/big_2.png"
        />

        <div className={style.cards_2}>
          <Card
            title="Lorem ipsum dolor sit amet consectetur et dolor cavis"
            content="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
            imgSrc="./img/campaign/small_1.png"
          />
          <Card
            title="Lorem ipsum dolor sit amet consectetur et dolor cavis"
            content="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
            imgSrc="./img/campaign/small_2.png"
          />
          <Card
            title="Lorem ipsum dolor sit amet consectetur et dolor cavis"
            content="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
            imgSrc="/img/campaign/small_3.png"
          />
        </div>

        {/* SONIC */}
        <Parallax
          className={style.sonic}
          translateX={["-60%", "-35%"]}
          translateY={["-1%", "2%"]}
          rotate={[-20, 20]}
          easing="easeInQuad"
        >
          <img src="./img/campaign/sonic.svg" alt="sonic" />
        </Parallax>
      </div>
    </div>
  );
};

export default Cards;
