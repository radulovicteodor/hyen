import * as style from "./SignUpCard.module.css";

const SignUpCard = ({ number, title, subtitle }) => {
  return (
    <div className={style.sign_up_card}>
      <div className={style.number}>{number}</div>
      <div className={style.texts}>
        <h4>{title}</h4>
        <p>{subtitle}</p>
      </div>
    </div>
  );
};

export default SignUpCard;
