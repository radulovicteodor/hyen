import * as style from "./SignUp.module.css";
import { motion } from "framer-motion";
import SignUpCard from "./SignUpCard/SignUpCard";
import { Parallax } from "react-scroll-parallax";

const SignUp = () => {
  const cards = [
    {
      number: 1,
      title: (
        <>
          Create/login <br /> to CA account
        </>
      ),
      subtitle:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet.",
    },
    {
      number: 2,
      title: (
        <>
          Complete the <br /> Alpha sign up form
        </>
      ),
      subtitle:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet.",
    },
    {
      number: 3,
      title: (
        <>
          Link your <br /> Steam Account
        </>
      ),
      subtitle:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet.",
    },
  ];

  return (
    <div className={style.sign_up_container}>
      <h1>Get early access and Sign up to the Alpha now</h1>
      <button>
        <span>Sign up to the ALPHA</span>
      </button>

      <div className={style.cards}>
        {cards.map((x) => {
          return (
            <SignUpCard
              key={x.number}
              number={x.number}
              title={x.title}
              subtitle={x.subtitle}
            />
          );
        })}
      </div>

      {/*  */}
      <Parallax
        className={style.hotdog}
        translateX={["0px", "150px"]}
        translateY={["500px", "150px"]}
        scale={[0.3, 1]}
        rotate={[-180, 30]}
        easing="easeInQuad"
      >
        <img src="./img/campaign/hotdog.png" />
      </Parallax>
      <div className={style.separator}>
        <motion.div
          initial={{ x: -172 }}
          animate={{ x: ["-100vw", "100vw"] }}
          transition={{ repeat: Infinity, duration: 5 }}
        ></motion.div>
      </div>
    </div>
  );
};

export default SignUp;
