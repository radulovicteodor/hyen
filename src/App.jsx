import "./App.css";
import { Header, Footer } from "./components";
import Layout from "./layouts/Layout";
import AppRoutes from "./Routes";
import { ParallaxProvider } from "react-scroll-parallax";

function App() {
  return (
    <>
      <Header />
      <Layout>
        <ParallaxProvider>
          <AppRoutes />
        </ParallaxProvider>
      </Layout>
      <Footer />
    </>
  );
}

export default App;
